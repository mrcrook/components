<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;
use Bitrix\Main\Context,
	Bitrix\Main\Type\DateTime,
	Bitrix\Main\Loader,
	Bitrix\Iblock;

if(!isset($arParams["CACHE_TIME"]))
	$arParams["CACHE_TIME"] = 36000000;

$arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);
$arParams["IBLOCK_ID"] = trim($arParams["IBLOCK_ID"]);

$arFilter = array (
	"IBLOCK_ID" => $arParams["IBLOCK_ID"],
	"ACTIVE" => "Y",
	"CHECK_PERMISSIONS" => "N",
);

$arSort = array(
	"SORT"=>"ASC",
    "ID"=>"DESC",
);
        
$arSelect = array(
    "*",
    "PROPERTY_REGION_CODE",
    "PROPERTY_SOCIAL_ICO",
    "PROPERTY_MAIL",
    "PROPERTY_PHONE1",
    "PROPERTY_PHONE2",
    "PROPERTY_ADDRESS",
);

if($_COOKIE["CITY_CODE"]) {
    $city = $_COOKIE["CITY_CODE"];
} else {
    $record = geoip_record_by_name($_SERVER["REMOTE_ADDR"]); // "31.162.223.127");//$_SERVER["REMOTE_ADDR"]
    if ($record) {
        $city = geoip_region_name_by_code($record["country_code"], $record["region"]);
    }
}

$addr = explode(".",$_SERVER["HTTP_HOST"]);
if(count($addr)==3) {
    $city_domain = $addr[0];
    $domain = $addr[1].".".$addr[2];
} else { 
    $city_domain = "";
    $penult = false; $last = false;
    foreach($addr as $i) {
        $penult = $last;
        $last = $i;
    }    
    $domain = $penult.".".$last;
}

if ((isset($_SERVER['REQUEST_SCHEME']) AND $_SERVER['REQUEST_SCHEME'] === 'https') OR (isset($_SERVER['HTTPS']) AND $_SERVER['HTTPS'] === 'on'))
    $protocol = 'https://';
else
    $protocol = 'http://';

if($_GET["c"]) {
    if(intVal($_GET["c"]) != $_COOKIE["CONFIRM_CITY"]) {
        $obElement = CIBlockElement::GetList($arSort, $arFilter+array("ID"=>intVal($_GET["c"])), false, false, $arSelect)->GetNext();
        if($obElement["PROPERTY_REGION_CODE_VALUE"])
            $city = $obElement["PROPERTY_REGION_CODE_VALUE"];
    }
    setcookie("CONFIRM_CITY", intVal($_GET["c"]), false, "/", ".".$domain); 
    $_COOKIE["CONFIRM_CITY"] = intVal($_GET["c"]);    
}

if($this->startResultCache(false, array($city_domain, $city, $protocol, $_COOKIE["CONFIRM_CITY"])))
{
	if(!Loader::includeModule("iblock"))
	{
        $this->abortResultCache();
        ShowError(GetMessage("IBLOCK_MODULE_NOT_INSTALLED"));
        return;
	}

	$arResult["ITEMS"] = array();
	$rsElement = CIBlockElement::GetList($arSort, $arFilter, false, false, $arSelect);
	while($obElement = $rsElement->GetNext())
	{
		$arResult["ITEMS"][] = $obElement;
        if($city == $obElement["PROPERTY_REGION_CODE_VALUE"] || !$arResult["CITY"]) {
            $arResult["CITY"] = $obElement;
            setcookie("CITY_CODE", $obElement["PROPERTY_REGION_CODE_VALUE"], false, "/", ".".$domain);
            $_COOKIE["CITY_CODE"] = $obElement["PROPERTY_REGION_CODE_VALUE"];
        }
	}

    if($arResult["CITY"]["CODE"] != $city_domain) {
        if(strlen($arResult["CITY"]["CODE"]))
            LocalRedirect($protocol.$arResult["CITY"]["CODE"].".".$domain.$_SERVER["REQUEST_URI"]);
        else
            LocalRedirect($protocol.$domain.$_SERVER["REQUEST_URI"]);
    }    

	$this->setResultCacheKeys();
	$this->includeComponentTemplate();
}
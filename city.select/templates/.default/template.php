<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="slogan">
        <a href="" onclick="$('.hint_city.b2').show();return false;"><span class="hidden-xs">Город: </span><?=$arResult["CITY"]["NAME"]?></a>
        <div class="hint_city b1">
            <div class="hint_city_content">Выбран ваш город?</div>
            <div class="hint_button">
                <a class="button btn btn-default" onclick="selectCity(<?=$arResult["CITY"]["ID"]?>); return false;">Да</a>
                <a class="button btn btn-default" onclick="$('.hint_city.b1').hide();$('.hint_city.b2').show();return false;">Нет</a>
            </div>
            <i class="corner"></i>
        </div>
        <div class="hint_city b2">
            <div class="hint_city_content">Выберите свой город</div>
            <div class="hint_button">
                <?foreach($arResult["ITEMS"] as $arItem){?>
                <a class="button transparent btn btn-default" onclick="selectCity(<?=$arItem["ID"]?>); return false;"><?=$arItem["NAME"]?></a>
                <?}?>
            </div>
            <i class="corner"></i>
        </div>
        <script>
        function selectCity(cityId){
            window.location.href = "?c="+cityId;
            $(".hint_city").hide();
        }
        function getCookie(name) {
          var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
          ));
          return matches ? decodeURIComponent(matches[1]) : undefined;
        }
        if(!getCookie("CONFIRM_CITY")) $(".slogan .b1").show();
        </script>        
</div>